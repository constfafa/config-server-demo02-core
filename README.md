1. 添加依赖
	compile('org.springframework.boot:spring-boot-starter-amqp')
	compile('org.springframework.cloud:spring-cloud-bus')
	compile('org.springframework.cloud:spring-cloud-stream-binder-rabbit')
2. 使用@RefreshScope
	约定：当使用@Value注解读取配置文件时，要在该bean的class上添加@RefreshScope注解
